
let firstname = document.getElementById('1name'),
    Lastname =  document.getElementById('lname'),
    Age =  document.getElementById('age'),
    searchInput =  document.getElementById('ser'),
    isAdmin = false;

document.addEventListener('DOMContentLoaded', ()=>{
    let searchFilter;
    searchInput.addEventListener('change',(e)=>{
        searchFilter = e.target.value;
        showUsers(searchFilter);
        console.log(e.target.value)
    })

    showUsers(searchFilter)
    document.getElementById("save").onclick = async function(){
        const users = await fetch("/user/all").then(resp => resp.json());

        await fetch(
            "/user",
            {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    firstname: firstname.value,
                    Lastname: Lastname.value,
                    Age: Age.value,
                    isAdmin: isAdmin //Boolean
                })
            }
        ).then(resp => resp.json())

        showUsers(searchFilter)
    }



    async function showUsers(filter){
        let showtable;
        if(!filter){
            showtable = await fetch("/user/all")
                .then(response => response.json())
                .then(json => json)
        } else{
            showtable = await fetch(`/user/all?filter=${filter}`)
                .then(response => response.json())
                .then(json => json)
        }


        document.getElementById('fn').innerHTML = '';
        document.getElementById('id').innerHTML = '';
        document.getElementById('ln').innerHTML = '';
        document.getElementById('ag').innerHTML = '';
        document.getElementById('isa').innerHTML = '';
        document.getElementById('del').innerHTML = '';
        showtable.map((el)=>{
            document.getElementById('fn').insertAdjacentHTML('beforeEnd', `<p>${el.firstname}</p>`);
            document.getElementById('id').insertAdjacentHTML('beforeEnd', `<p>${el._id}</p>`);
            document.getElementById('ln').insertAdjacentHTML('beforeEnd', `<p>${el.Lastname}</p>`);
            document.getElementById('ag').insertAdjacentHTML('beforeEnd', `<p>${el.Age}</p>`);
            document.getElementById('isa').insertAdjacentHTML('beforeEnd', `<p>${el.isAdmin}</p>`);

            let newButton = document.createElement("button");
            newButton.innerHTML = '&#128465;'
            newButton.addEventListener('click', async ()=>{
                await fetch(
                    "/user",
                    {
                        method: "DELETE",
                        headers: {
                            "Content-type": "application/json"
                        },
                        body: JSON.stringify({
                            name: el.firstname,
                        })
                    }
                ).then(resp => {
                    showUsers();
                    return resp.json();

                })

            })
            document.getElementById('del').insertAdjacentElement('beforeEnd', newButton);
        })

    }


});
