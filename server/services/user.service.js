const MongoService = require("./mongo.service");

class UserService{

    constructor() {
        this.userTable = MongoService.collections.users;
        if(!this.userTable){
            throw new Error("Collection user does not exist!");
        }
    }

    async getAllUsers(){
        return await this.userTable.find({}).toArray();
    }

    async getFilteredUsers(filter){
        return await this.userTable.find({Lastname: filter}).toArray();
    }


    async setUser(user) {
        return await this.userTable.update(
            {firstname: user.name},
            {$set: user},
            {upsert: true}
        );
    }

    async deleteUserByName(user) {
        return await this.userTable.deleteOne({firstname: user.name});
    }
}

module.exports = UserService;